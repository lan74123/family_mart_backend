<?php
require '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

$dotenv = new Dotenv\Dotenv(__DIR__. '/..');
$dotenv->load();
$response = new JsonResponse();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
]);

$server = getenv('APP_URL');
$error_message = '';
$result = false;
$playerID = 0;

$user_path = '';
    if ($request->getMethod() == 'POST')
    {
        if (empty($error_message))
        {
            $playerID = $request->request->get('playerID');
            if($playerID === null){
                $kol = $db->select('player', [
                    'id(playerID)',
                    'name',
                    // 'fbname',
                    // 'fbpic',
                    'content',
                    'musictype',
                    'animationtype',
                    'kol'
                ],
                [
                    'valid' => 1,
                    'kol' => 1,
                    'ORDER' =>['id' => 'DESC']
                ]);

                $kol_count = count($kol);

                $list = $db->select('player', [
                        'id(playerID)',
                        'name',
                        // 'fbname',
                        // 'fbpic',
                        'content',
                        'musictype',
                        'animationtype',
                        'kol'
                    ],
                    [
                        'valid' => 1,
                        'kol' => 0,
                        'ORDER' =>['id' => 'DESC'],
                        'LIMIT' => 10-$kol_count
                    ]);
                $list = array_merge($kol,$list);
            }
            else{
                $list = $db->select('player', [
                    'id(playerID)',
                    'name',
                    // 'fbname',
                    // 'fbpic',
                    'content',
                    'musictype',
                    'animationtype',
                    'kol'
                ],
                [
                    'valid' => 1,
                    'id[<]' => $playerID,
                    'kol' => 0,
                    'ORDER' =>['id' => 'DESC'],
                    'LIMIT' => 10
                ]);
            }
            
            $result = "Y";

        }
    }
    else
    {
        $error_message = 'Method not allow';
    }

    if (empty($error_message))
    {
        $data = array('Result' => $result, 
        'data' =>  $list,
        'ErrorMessage' => $error_message);
    }
    else
    {
        $data = array('Result' => "N", 
        'ErrorMessage' => $error_message);
    }

    $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
    $response->setData($data);
    //$response->setCallback('handleResponse');
    $response->send();
