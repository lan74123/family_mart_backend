<?php
require 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
	'password' => getenv('DB_PASSWORD'),
]);
$meta = [
    'description' => '',
    'image' => ''
];


if ($request->query->has('meta'))
{
    $meta = explode('_', $request->query->get('meta'));
    $fbid = $meta[0];
    $no = $meta[1];
    $ts = $meta[2];
}
else
{
    $fbid = $request->query->has('fbid') ? $request->query->get('fbid') : '0';
    $no = $request->query->has('no') ? $request->query->get('no') : '0';
    $ts = $request->query->has('ts') ? $request->query->get('ts') : '0';
}


$result = $db->select('user_logs', '*', ['fbid' => $fbid, 'id' => $no, 'user_image[~]' => $ts]);



$time_point = new DateTime('2018-02-27 00:00:00');
$time_now = new DateTime('2018-03-01 01:40:00');
if ($time_now > $time_point)
{
	$diff = $time_point->diff($time_now);

	$minutes = $diff->days * 24 * 60;
	$minutes += $diff->h * 60;
	$minutes += $diff->i;

	$time_add_people = floor($minutes / 5) * 5;
}

if (count($result) > 0)
{
    $meta['description'] = '目前已經蒐集'.($result[0]['id'] + 100 + $time_add_people).'位新年輕人，驚喜即將發生！';
    $meta['image'] = getenv('APP_URL').'user/'.$fbid.'/'.$ts.'_result.jpg';
}
else
{
    $response = new RedirectResponse('/');
    //$response->send();
}

?>
<!DOCTYPE html>
<html lang="zh-Hant-TW" prefix="og: http://ogp.me/ns#">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta property="fb:app_id" content="<?php getenv('APP_ID') ?>" />
    <meta property="og:url" content="<?php echo $request->getUri() ?>" />
    <meta property="og:type" content="website" />
    <meta property="og:title" content="測出你內心的年輕魂，成為「新年輕人」！" />
    <meta property="og:description" content="<?php echo $meta['description'] ?>" />
    <meta property="og:image" content="<?php echo $meta['image'] ?>" />
    <meta property="og:image:type" content="image/jpeg" />
    <meta property="og:image:width" content="1200" />
    <meta property="og:image:height" content="630" />
    <title>測出你內心的年輕魂，成為「新年輕人」！</title>
</head>
<body>
    Redirecting...
</body>
<script>
setTimeout(function(){
    document.location.href = '<?php echo getenv('REDIRECT_URL') ?>'
}, 1500)
</script>
</html>