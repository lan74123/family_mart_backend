<?php
require '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

$dotenv = new Dotenv\Dotenv(__DIR__. '/..');
$dotenv->load();
$response = new JsonResponse();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
]);

$server = getenv('APP_URL');
$error_message = '';
$result = false;
$page = 1;
$data_per_page = 10;

$user_path = '';
    if ($request->getMethod() == 'POST')
    {
        if (!$request->request->has('name'))
        {
            $error_message .= '參數錯誤\\n';
        }
        

        if (empty($error_message))
        {
            $name = $request->request->get('name');
            if ($request->request->has('page')){
                $page = $request->request->get('page');
            }
            $list = $db->select('player', [
                'id(playerID)',
                'name',
                'fbname',
                'fbpic',
                'content',
                'musictype',
                'animationtype',
                'kol'
            ],
            [
                'valid' => 1,
                'name[~]' => $name,
                'LIMIT' => [($page-1)*$data_per_page,$data_per_page],
                'ORDER' =>[
                    'kol' => 'DESC',
                    'id' => 'DESC'
                ]
            ]);
            
            $result = "Y";

        }
    }
    else
    {
        $error_message = 'Method not allow';
    }

    if (empty($error_message))
    {
        $data = array('Result' => $result, 
        'data' =>  $list,
        'ErrorMessage' => $error_message);
    }
    else
    {
        $data = array('Result' => "N", 
        'ErrorMessage' => $error_message);
    }

    $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
    $response->setData($data);
    //$response->setCallback('handleResponse');
    $response->send();
