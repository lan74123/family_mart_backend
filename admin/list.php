<?php

require (realpath(__DIR__ . '/..')).'/vendor/autoload.php';
require (realpath(__DIR__ . '/..')).'/login.php';


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use Medoo\Medoo;

ini_set("display_errors", "On"); // 設定是否顯示錯誤( On=顯示, Off=隱藏 )
ini_set('memory_limit', '350M');

$dotenv = new Dotenv\Dotenv(realpath(__DIR__ . '/..'));
$dotenv->load();
$request = Request::createFromGlobals();
$db = new Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8'
]);

function whereOrAndHelper(String $query){
    //如果有 where
    if(strpos($query, 'where') !== false){
        return " and ";
    }
    else{
        return " where ";
    }
}
// if ($request->getClientIp() != "122.147.249.2")
// {
//     exit;
// }

$root = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]";


$valid = $request->query->has('valid') ? $request->query->get('valid') : null;
$name = $request->query->has('name') ? $request->query->get('name') : null;
$fbname = $request->query->has('fbname') ? $request->query->get('fbname') : null;
$hasmobile = $request->query->has('hasmobile') ? $request->query->get('hasmobile') : null;
$mobile = $request->query->has('mobile') ? $request->query->get('mobile') : null;
$fbshare = $request->query->has('fbshare') ? $request->query->get('fbshare') : null;
$date_start = $request->query->has('date_start') ? $request->query->get('date_start') : null;
$date_end = $request->query->has('date_end') ? $request->query->get('date_end') : null;

$where_query='';
if(!is_null($valid) && $valid != ''){
    $where_query.= whereOrAndHelper($where_query)."valid = ".$db->quote($valid);
}
if(!is_null($hasmobile) && $hasmobile != ''){
    if($hasmobile == "0"){
        $where_query.= whereOrAndHelper($where_query)."mobile is null ";
    }
    else{
        $where_query.= whereOrAndHelper($where_query)."mobile != '' ";
    }
}
if(!empty($name)){
    $where_query.= whereOrAndHelper($where_query)."name like".$db->quote('%'.$name.'%');
}
if(!empty($fbname)){
    $where_query.= whereOrAndHelper($where_query)."fbname like".$db->quote('%'.$fbname.'%');
}
if(!empty($mobile)){
    $where_query.= whereOrAndHelper($where_query)."mobile = ".$db->quote($mobile);
}
if(!is_null($fbshare) && $fbshare != ''){
    $where_query.= whereOrAndHelper($where_query)."fbshare = ".$db->quote($fbshare);
}
if(!empty($date_start)){
    $where_query.= whereOrAndHelper($where_query)."created_at >= ".$db->quote($date_start);
}
if(!empty($date_end)){
    $time_original = strtotime($date_end);
    $time_add      = $time_original + (3600*24); //add seconds of one day
    $new_date      = date("Y-m-d", $time_add);
    $where_query.= whereOrAndHelper($where_query)."created_at <= ".$db->quote($new_date);
}

$data_per_page = 30;

$now_page = $request->query->has('page') ? $request->query->get('page') : 1;

$data_start = $data_per_page * ($now_page - 1);

$result = $db->select('player','*',
Medoo::raw('
    '.$where_query.'
    ORDER BY created_at DESC
    LIMIT '.$data_start.','.$data_per_page.'
')
);

$excel_result = $db->select('player','*',
Medoo::raw('
    '.$where_query.'
    ORDER BY created_at DESC    
')
);

// $debug = $db->debug()->select('player','*',
// Medoo::raw('
//     '.$where_query.'
//     ORDER BY created_at DESC
//     LIMIT '.$data_start.','.$data_per_page.'
// ')
// );

$total_count = $db->count('player','*',
Medoo::raw('
    '.$where_query.'
')
);

$page = floor($total_count / $data_per_page) + 1;

if(isset($_GET['outputExcel']) && $_GET['outputExcel'] == "outputExcel") {
    downloadExcel();
}    
function downloadExcel()
{
    $spreadsheet = new Spreadsheet();

    global $excel_result;
    $fileName = 'family_mart_excel_'.date("Y-m-d_h_i_s", time());

    try{

        $titles=['有效否' , '#' , '暱稱' ,'FB大頭貼' , 'FB姓名' , '手機' , '已分享' , '建立日期' , 'utm_source' , 'utm_medium' , 'utm_campaign'];
        $datas=array();
        $i = 2;
        foreach($excel_result as $row)
        {
            if($row['fbpic'] != null){
                $picID = str_replace('.jpg','',basename($row['fbpic']));
                $fbpic_path = '../storage/FBpic/'.$picID.'.jpg';
                if(file_exists($fbpic_path)){
                    $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $drawing->setName('fbpic');
                    $drawing->setDescription('fbpic');
                    $drawing->setPath('../storage/FBpic/'.$picID.'.jpg');
                    $drawing->setCoordinates('D'.$i);
                    $drawing->setHeight(100); 
                    // $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(100);
                    $drawing->setWorksheet($spreadsheet->getActiveSheet()); 
                }
            }
            $spreadsheet->getActiveSheet()->getRowDimension($i)->setRowHeight(100);


            $rowData=array();
            array_push($rowData,( $row['valid'] == '1' )? '有效':'無效');           
            array_push($rowData,$row['id']);
            array_push($rowData,$row['name']);
            array_push($rowData,$row['fbpic']);
            array_push($rowData,$row['fbname']);
            array_push($rowData,$row['mobile']);
            array_push($rowData,( $row['fbshare'] == '1' )? '已分享':'未分享');           
            array_push($rowData,$row['created_at']);
            array_push($rowData,$row['utm_source']);
            array_push($rowData,$row['utm_medium']);
            array_push($rowData,$row['utm_campaign']);
            array_push($datas,$rowData);
            $i++;
        }              

        $spreadsheet->getActiveSheet()
        ->fromArray(
            $titles,  // The data to set
            NULL,        // Array values with this value will not be set
            'A1'         // Top left coordinate of the worksheet range where
                    //    we want to set these values (default is A1)
        );

        $spreadsheet->getActiveSheet()
        ->fromArray(
            $datas,  // The data to set
            NULL,        // Array values with this value will not be set
            'A2'         // Top left coordinate of the worksheet range where
                    //    we want to set these values (default is A1)
        );
        $spreadsheet->getActiveSheet()->getPageSetup()->setFitToHeight(1);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('D')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('E')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('F')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('G')->setWidth(10);
        $spreadsheet->getActiveSheet()->getColumnDimension('H')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('I')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('J')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('K')->setWidth(20);

        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$fileName.'.xlsx"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;
    }catch(Exception $e){
        echo $e;
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>作品管理後台</title>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
        crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
        crossorigin="anonymous"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.js"
        integrity="sha256-ncetQ5WcFxZU3YIwggfwOwmewLVX4SHLBtDYnrsxooY=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.standalone.css"
        integrity="sha256-uN+x99pofVuHMbma2OauPsPOP6Y3bYewUszGyStlT28=" crossorigin="anonymous" />
</head>

<body>
    <div class="container" id="sandbox-container">
        <div class="row">
            <div class="col-lg-12">
                <div class="card bg-info text-white">
                    <div class="card-header">
                        作品管理
                        <a href="../logout.php"><button type="submit" class="btn btn-danger btn-xs" style="">登出</button></a>
                    </div>
                    <div class="car-body">
                        <div class="col-md-12">
                            <form method="get" id="list" action="list.php">
                                <div class="input-group-sm mb-3">
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <label>有效否</label>
                                            <select class="custom-select" name="valid">
                                                <option value="">不拘</option>
                                                <option <?php if($valid=='1' ){?>selected = "selected"
                                                    <?php } ?> value="1">有效</option>
                                                <option <?php if($valid=='0' ){?>selected = "selected"
                                                    <?php } ?> value="0">無效</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>有填寫手機</label>
                                            <select class="custom-select" name="hasmobile">
                                                <option value="">不拘</option>
                                                <option <?php if($hasmobile=='1' ){?>selected = "selected"
                                                    <?php } ?> value="1">有</option>
                                                <option <?php if($hasmobile=='0' ){?>selected = "selected"
                                                    <?php } ?> value="0">無</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-2">
                                            <label>暱稱</label>
                                            <input class="form-control" name="name" type="text" value="<?php echo $name?>">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>FB姓名</label>
                                            <input class="form-control" name="fbname" type="text" value="<?php echo $fbname?>">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>手機</label>
                                            <input class="form-control" name="mobile" type="text" value="<?php echo $mobile?>">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>建立日期</label>
                                            <input class="form-control datepicker" name="date_start" type="text"
                                                autocomplete="off" value="<?php echo $date_start?>">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>結束日期</label>
                                            <input class="form-control datepicker" name="date_end" type="text"
                                                autocomplete="off" value="<?php echo $date_end?>">
                                        </div>
                                        <div class="col-sm-2">
                                            <label>是否已分享</label>
                                            <select class="custom-select" name="fbshare">
                                                <option value="">不拘</option>
                                                <option <?php if($fbshare=='0' ){?>selected = "selected"
                                                    <?php } ?> value="0">未分享</option>
                                                <option <?php if($fbshare=='1' ){?>selected = "selected"
                                                    <?php } ?> value="1">已分享</option>
                                            </select>
                                        </div>
                                        <div class="col-sm-4">
                                            <button type="submit" class="btn btn-primary btn-xs " style="margin-top:30px;">篩選</button>
                                            <button type="button" onclick="exportExcel()" class="btn btn-success btn-xs"
                                                style="margin-top:30px;">匯出Excel</button>
                                            <input type="hidden" name="outputExcel" disabled />
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col" style="overflow: auto;">
            <div>
                <p>共<?php echo $total_count?>筆資料</p>
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" href="list.php?page=1&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">Top</a></li>
                    <li class="page-item"><a class="page-link" href="list.php?page=<?php echo ($now_page - 1) ?>&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">Previous</a></li>
                    <?php for ($i = $now_page-5 > 0 ? $now_page-5:0; $i < ($now_page+5 > 10 ? $now_page+5:10); $i++) {
                        if($i>=$page)  break; 
                    ?>
                    <li class="page-item <?php echo ($i+1 == $now_page) ? 'active' : '' ?>"><a class="page-link"
                            href="list.php?page=<?php echo $i+1 ?>&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">
                            <?php echo $i+1 ?></a></li>
                    <?php } ?>
                    <li class="page-item"><a class="page-link" href="list.php?page=<?php echo ($now_page + 1) ?>&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">Next</a></li>
                    <li class="page-item"><a class="page-link" href="list.php?page=<?php echo $page ?>&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">End</a></li>
                </ul>
            </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>有效</th>
                            <th>#</th>
                            <th>暱稱</th>
                            <th>FB大頭貼</th>
                            <th>FB姓名</th>
                            <th>手機</th>
                            <th>已分享</th>
                            <th>建立日期</th>
                            <th>動作</th>
                            <th>utm_source</th>
                            <th>utm_medium</th>
                            <th>utm_campaign</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($result as $row) { ?>
                        <tr>
                            <?php
                                if($row['valid'] == 1 )
                                    echo "<td><i class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i></td>";
                                else
                                    echo "<td><i class=\"fa fa-square-o\" aria-hidden=\"true\"></i></td>";
                            ?>
                            <td>
                                <?php echo $row['id'] ?>
                            </td>
                            <td>
                                <?php echo $row['name'] ?>
                            </td>
                            <td><img src="<?php echo $row['fbpic'] ?>" width="100px" /></td>
                            <td>
                                <?php echo $row['fbname'] ?>
                            </td>
                            <td>
                                <?php echo $row['mobile'] ?>
                            </td>
                            <?php
                                if($row['fbshare'] == 1 )
                                    echo "<td><i class=\"fa fa-check-square-o\" aria-hidden=\"true\"></i></td>";
                                else
                                    echo "<td><i class=\"fa fa-square-o\" aria-hidden=\"true\"></i></td>";
                            ?>
                            <td>
                                <?php
                                        $date = new DateTime($row['created_at']);
                                        echo $date->format('Y-m-d H:i:s');
                                    ?>
                            </td>
                            <td>
                                <!-- <a href="detail.php?playerID=<?php //echo $row['id'] ?>"><button type="button" class="btn btn-primary btn-xs">詳細</button></a> -->
                                <a href="valid.php?playerID=<?php echo $row['id'] ?>">
                                    <?php
                                    if($row['valid'] == 1 )
                                        echo "<button type=\"button\" class=\"btn btn-danger btn-xs\">下架</button>";
                                    else
                                        echo "<button type=\"button\" class=\"btn btn-success btn-xs\">上架</button>";
                                    ?>
                                    <a href="../player.html?id=<?php echo $row['id'] ?>" target="_blank"><button type="button"
                                            class="btn btn-primary btn-xs">檢視</button></a>
                                </a>
                            </td>
                            <td>
                                <?php echo $row['utm_source'] ?>
                            </td>
                            <td>
                                <?php echo $row['utm_medium'] ?>
                            </td>
                            <td>
                                <?php echo $row['utm_campaign'] ?>
                            </td>
                        </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <div>
                    <ul class="pagination">
                        <li class="page-item"><a class="page-link" href="list.php?page=1&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">Top</a></li>
                        <li class="page-item"><a class="page-link" href="list.php?page=<?php echo ($now_page - 1) ?>&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">Previous</a></li>
                        <?php for ($i = $now_page-5 > 0 ? $now_page-5:0; $i < ($now_page+5 > 10 ? $now_page+5:10); $i++) {
                            if($i>=$page)  break; 
                        ?>
                        <li class="page-item <?php echo ($i+1 == $now_page) ? 'active' : '' ?>"><a class="page-link"
                                href="list.php?page=<?php echo $i+1 ?>&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">
                                <?php echo $i+1 ?></a></li>
                        <?php } ?>
                        <li class="page-item"><a class="page-link" href="list.php?page=<?php echo ($now_page + 1) ?>&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">Next</a></li>
                        <li class="page-item"><a class="page-link" href="list.php?page=<?php echo $page ?>&valid=<?php echo $valid?>&hasmobile=<?php echo $hasmobile?>&name=<?php echo $name?>&fbname=<?php echo $fbname?>&mobile=<?php echo $mobile?>&fbshare=<?php echo $fbshare?>&date_start=<?php echo $date_start?>&date_end=<?php echo $date_end?>">End</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</body>
<script>
    $(document).ready(function () {
        <?php 
            $memory_limit = ini_get('memory_limit');
            if (preg_match('/^(\d+)(.)$/', $memory_limit, $matches)) {
                if ($matches[2] == 'M') {
                    $memory_limit = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
                } else if ($matches[2] == 'K') {
                    $memory_limit = $matches[1] * 1024; // nnnK -> nnn KB
                }
            }
            ?>
        console.log('current memory usage = '+<?php echo memory_get_usage(); ?>);
        console.log('current memory limit = '+<?php echo $memory_limit ?>);
        $('.datepicker').datepicker({
            format: "yyyy/mm/dd",
            todayHighlight: true,
            clearBtn: true,
            autoclose: true,
            language: "zh-TW"
        });
        $('.datepicker').datepicker();
    });

    function exportExcel() {
        $('input[name=outputExcel]').prop("disabled", false);
        $('input[name=outputExcel]').val('outputExcel');
        $('#list').submit();
        $('input[name=outputExcel]').prop("disabled", true);
    }
</script>

</html>