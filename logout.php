<?php

    session_start();
    $_SESSION['loggedIn'] = false;

    echo 'You have logged out';  

    header('Location: ' . $_SERVER['HTTP_REFERER']);
    die();

?>
