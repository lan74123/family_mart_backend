<?php
require (realpath(__DIR__ . '/..')).'/vendor/autoload.php';
require (realpath(__DIR__ . '/..')).'/login.php';


use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

$dotenv = new Dotenv\Dotenv(__DIR__. '/..');
$dotenv->load();
$response = new JsonResponse();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
]);

$server = getenv('APP_URL');
$result = false;
$ts = time();
$playerID = 0;

$user_path = '';

    //驗證碼輸入正確    
    if ($request->getMethod() == 'GET')
    {

        if (empty($error_message))
        {
            $playerID = $request->query->get('playerID');

            $valid = $db->get('player','valid',
            [
                'id' => $playerID
            ]);

            $toggle_valid= ($valid == '1') ? '0' : '1'; 

            $db->update('player', [
                'valid' => $toggle_valid
            ],
            [
                'id' => $playerID
            ]);
            
        }
        // echo 'toggle_valid='.$toggle_valid;
        // echo 'valid='.$valid;
        header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    else
    {
        $error_message = 'Method not allow';

    }
