<?php
require (realpath(__DIR__)).'/vendor/autoload.php';
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
$request = Request::createFromGlobals();
//測試
// $client_ip = substr($request->getClientIp(),0,8);
//正式
// $client_ip = substr($request->getClientIp(),0,6);
// $allowed_ip_array = array('172.31');
if (!empty($_SERVER["HTTP_CLIENT_IP"])){
    $ip = $_SERVER["HTTP_CLIENT_IP"];
}elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
    $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
}else{
    $ip = $_SERVER["REMOTE_ADDR"];
}

$allowed_ip_array = array('::1','122.146.','122.147.','172.31.1');
$client_ip = substr($ip,0,8);

if (!in_array($client_ip,$allowed_ip_array)) {
	// echo 'my='.$client_ip;
	// foreach($allowed_ip_array as $item)
	// 	echo 'allow='.$item;
    header("Location: https://www.familymart30.com.tw/");
    exit();
   }
$dotenv = new Dotenv\Dotenv(realpath(__DIR__));
$dotenv->load();

//define('URL', 'http://'.$_SERVER['HTTP_HOST']);
//header('Location: '.URL.'/admin/list.php', TRUE, 302);


$hash = getenv('ADMIN_PASSWORD');
session_start();
if (!isset($_SESSION['loggedIn'])) {
    $_SESSION['loggedIn'] = false;
}

if (isset($_POST['password'])) {
    if (password_verify($_POST['password'], $hash)) {
        $_SESSION['loggedIn'] = true;
    } else {
        echo "<script>alert('密碼錯誤')</script>";
        //die ('Incorrect password');
    }
} 

if (!$_SESSION['loggedIn']): ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Login</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="../images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<!-- <link rel="stylesheet" type="text/css" href="../fonts/Linearicons-Free-v1.0.0/icon-font.min.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../vendor/animate/animate.css">
<!--===============================================================================================-->	
	<!-- <link rel="stylesheet" type="text/css" href="../vendor/css-hamburgers/hamburgers.min.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<!-- <link rel="stylesheet" type="text/css" href="../vendor/daterangepicker/daterangepicker.css"> -->
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="../css/util.css">
	<link rel="stylesheet" type="text/css" href="../css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100 p-l-55 p-r-55 p-t-65 p-b-50">
				<form method="post" class="login100-form validate-form">
					<span class="login100-form-title p-b-33">
						Account Login
					</span>

					<div class="wrap-input100 validate-input">
						<input class="input100" type="password" name="password" placeholder="Password">
						<span class="focus-input100-1"></span>
						<span class="focus-input100-2"></span>
					</div>

					<div class="container-login100-form-btn m-t-20">
						<button type="submit" name="submit" value="Login" class="login100-form-btn">
							Sign in
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
	

	
<!--===============================================================================================-->
	<script src="../vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="../vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="../vendor/bootstrap/js/popper.js"></script>
	<script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="../vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="../vendor/daterangepicker/moment.min.js"></script>
	<!-- <script src="../vendor/daterangepicker/daterangepicker.js"></script> -->
<!--===============================================================================================-->
	<!-- <script src="../vendor/countdowntime/countdowntime.js"></script> -->
<!--===============================================================================================-->
	<script src="/js/main.js"></script>

</body>
</html>
<?php
exit();
endif;
?>