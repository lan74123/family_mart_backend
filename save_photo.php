<?php
require 'vendor/autoload.php';
require 'helper/AzureFace.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Intervention\Image\ImageManagerStatic as Image;
use Azure\Face\Face;

Image::configure(array('driver' => 'imagick'));
$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();
$response = new JsonResponse();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
]);
$fb = new \Facebook\Facebook([
    'app_id' => getenv('APP_ID'),
    'app_secret' => getenv('APP_SECRET'),
    'default_graph_version' => 'v2.11',
    'default_access_token' => getenv('APP_ID') . '|' . getenv('APP_SECRET'),
]);

$server = getenv('APP_URL');
$err = '';
$err_code = '';
$result = false;
$ts = time();

$user_path = '';

if ($request->getMethod() == 'POST')
{
    if (!$request->request->has('fbid'))
    {
        $err_code = 'F00';
        $err .= 'FBID未輸入\\n';
    }
    if (!$request->files->has('image'))
    {
        if (!$request->request->has('image_url'))
        {
            $err_code = 'F00';
            $err .= '圖片未輸入\\n';
        }
    }
    if (!$request->request->has('options'))
    {
        $err_code = 'F00';
        $err .= '選項未輸入\\n';
    }

    //STEP1 Save Image
    if (empty($err))
    {
        $fbid = $request->request->get('fbid');
        $options = $request->request->get('options');
        $name = $request->request->has('name') ? $request->request->get('name') : '';
        $email = $request->request->has('email') ? $request->request->get('email') : '';

        $user_path = 'user/'.$fbid.'/';
        $log_txt = fopen($user_path.$ts.'_log.txt', 'w');
        $log_ts = time();


        $image_field = $request->files->has('image') ? $request->files->get('image') : $request->request->get('image_url');

        if (!file_exists($user_path))
        {
            mkdir($user_path);
        }

        try
        {
            $head = Image::make($image_field);
            $head->save($user_path.$ts.'_head.jpg');
            fwrite($log_txt, "STEP1 Save Image".(time() - $log_ts)."\n");
        }
        catch (\Exception $e)
        {
            $err = '照片儲存失敗';
            $err_code = 'I01';
            fclose($log_txt);
        }
    }

    //STEP2 Detect Face
    if (empty($err))
    {
        $log_ts = time();

        $face = new Face($server.$user_path.$ts.'_head.jpg');
        $face->detect();
        fwrite($log_txt, "STEP2-1 Azure:".(time() - $log_ts)."\n");
        $log_ts = time();

        if ($face->result)
        {
            $x = ($face->left > $face->width) ? $face->left - $face->width : 0;
            $y = ($face->top > $face->height) ? $face->top - $face->height : 0;

            if ($head->width() < $x + $face->width * 3)
            {
                $width = $head->width();
                $x = 0;
            }
            else
            {
                $width = $face->width * 3;
            }

            if ($head->height() < $y + $face->height * 3)
            {
                $height = $head->height();
                $y = 0;
            }
            else
            {
                $height = $face->height * 3;
            }

            $o = false;

            if ($width == $head->width() || $height == $head->height())
            {
                $o = true;
            }

            if ($width / $height < 1.3)
            {
                $y = $y + (($height - floor($width / 1.3)) / 2);
                $height = floor($width / 1.3);

                if ($o)
                {
                    if ($y > $face->top)
                    {
                        $y = $face->top;
                    }
                    if ($y < $face->top)
                    {
                        $y = $face->top;
                    }
                }
            }

            //舊版演算法
            /*
            $x = ($face->left > $face->width * 1.5) ? $face->left - $face->width * 1.5 : 0;
            $y = ($face->top > $face->height) ? $face->top - $face->height : 0;

            if ($head->width() < $x + $face->width * 4)
            {
                $width = $head->width();
                $x = 0;
            }
            else
            {
                $width = $face->width * 4;
            }

            if ($head->height() < $y + $face->height * 3)
            {
                $height = $head->height();
                $y = 0;
            }
            else
            {
                $height = $face->height * 3;
            }

            if ($y != 0)
            {
                $y = $y + floor($face->height / 2);
            }

            if ($x != 0 && $x > floor($face->width / 2))
            {
                $x = $x - floor($face->width / 2);
            }

            if ($x == 0 && $y == 0)
            {
                if ($width / $height < 1.3)
                {
                    $height = floor($width / 1.3);
                    $y = $face->top / 2;
                }
            }
            */

            try
            {
                $head->crop($width, $height, floor($x), floor($y));
                $head->getCore()->modulateImage(100, 50, 100);
                $head->save($user_path.$ts.'_head_crop.jpg');

                fwrite($log_txt, "STEP2-2 Save crop head:".(time() - $log_ts)."\n");
            }
            catch (\Exception $e)
            {
                //throw($e);
                $err = '照片儲存失敗';
                $err_code = 'I02';
                fclose($log_txt);
            }       
        }
        else
        {
            //$err .= $face->message.'\\n';
            $db->insert('user_logs', [
                'fbid' => $fbid,
                'name' => $name,
                'email' => $email,
                'age' => 9999,
                'gender' => 'N/A',
                'options' => $options,
                'user_image' => $ts.'_head.jpg',
                'result_image' => $ts.'_result.jpg',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
    
            $err_code = $face->message;
            $err = '臉部辨識失敗';
            fclose($log_txt);
        }
    }

    //STEP3 Make
    if (empty($err))
    {
        $log_ts = time();

        $ratio = 630 / $head->height();
        $head = $head->resize(floor($head->width() * $ratio) , 630);

        $txt1_1 = rand(1, 18);
        do {
            $txt1_2 = rand(1, 18);
        } while ($txt1_2 == $txt1_1);
        $txt2 = rand(1, 10);

        do {
            $per1 = rand(10, 25);
        } while ($per1 % 5 == 0);
        
        do {
            $per2 = rand($per1, 100 - $per1);
            $per3 = 100 - $per1 - $per2;
        } while ($per2 % 5 == 0 || $per3 % 5 == 0);

        $per1_1 = floor($per1 / 10);
        $per1_2 = $per1 % 10;
        $per2_1 = floor($per2 / 10);
        $per2_2 = $per2 % 10;
        $per3_1 = floor($per3 / 10);
        $per3_2 = $per3 % 10;
        $f_age = $face->age;
        $age_1 = floor($f_age / 10);
        $age_2 = $f_age % 10;

        $result = Image::canvas(1200, 630);

        $resize = 630 / $head->height();
        $resize_width = $head->width() * $resize;
        $head->resize(floor($resize_width), 630);

        $result->insert($head, 'right');
        $result->insert('assets/lancome_bg.png');

        $i_txt1_1 = Image::make('assets/txt1_'.$txt1_1.'.png');
        $result->insert($i_txt1_1, 'top-left', 60, 180);
        if ($per3_1 != 0)
        {
            $result->insert('assets/number_'.$per3_1.'.png', 'top-left', 60 + $i_txt1_1->width() + 20, 160);
        }
        $result->insert('assets/number_'.$per3_2.'.png', 'top-left', 60 + $i_txt1_1->width() + 60, 160);
        $result->insert('assets/number_percentage.png', 'top-left', 60 + $i_txt1_1->width() + 100, 160);

        $i_txt1_2 = Image::make('assets/txt1_'.$txt1_2.'.png');
        $result->insert($i_txt1_2, 'top-left', 60, 300);
        if ($per2_1 != 0)
        {
            $result->insert('assets/number_'.$per2_1.'.png', 'top-left', 60 + $i_txt1_2->width() + 20, 280);
        }
        $result->insert('assets/number_'.$per2_2.'.png', 'top-left', 60 + $i_txt1_2->width() + 60, 280);
        $result->insert('assets/number_percentage.png', 'top-left', 60 + $i_txt1_2->width() + 100, 280);

        $i_txt2 = Image::make('assets/txt2_'.$txt2.'.png');
        $result->insert($i_txt2, 'top-left', 60, 420);
        if ($per1_1 != 0)
        {
            $result->insert('assets/number_'.$per1_1.'.png', 'top-left', 60 + $i_txt2->width() + 20, 400);
        }
        $result->insert('assets/number_'.$per1_2.'.png', 'top-left', 60 + $i_txt2->width() + 60, 400);
        $result->insert('assets/number_percentage.png', 'top-left', 60 + $i_txt2->width() + 100, 400);

        $result->insert('assets/lancome_txt_rb.png');
        $result->insert('assets/number_'.$age_1.'.png', 'top-left', 900, 415);
        $result->insert('assets/number_'.$age_2.'.png', 'top-left', 940, 410);

        $result->save($user_path.$ts.'_result.jpg');

        fwrite($log_txt, "STEP3-1 Save result:".(time() - $log_ts)."\n");
        $log_ts = time();

        $err = '';

        $db->insert('user_logs', [
            'fbid' => $fbid,
            'name' => $name,
            'email' => $email,
            'age' => $face->age,
            'gender' => $face->gender,
            'options' => $options,
            'user_image' => $ts.'_head.jpg',
            'result_image' => $ts.'_result.jpg',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $no = $db->id();
        $image_url = $server.$user_path.$ts.'_result.jpg';
        $share_url = $server.'share.php?meta='.$fbid.'_'.$no.'_'.$ts;

        fwrite($log_txt, "STEP3-2 Insert DB:".(time() - $log_ts)."\n");
        $log_ts = time();

        try {
            $fb_response = $fb->get('/?id='.htmlentities($share_url));
        } catch(\Facebook\Exceptions\FacebookResponseException $e) {

        } catch(\Facebook\Exceptions\FacebookSDKException $e) {
        
        }

        fwrite($log_txt, "STEP3-3 Call FB:".(time() - $log_ts)."\n");
        fclose($log_txt);

        $result = true;
    }
}
else
{
    $err = 'Method not allow';
    $err_code = 'M00';
}

if (empty($err_code))
{
    $data = array('result' => $result, 'err_code' => $err_code, 'message' => $err, 'result' => array('age' => $f_age, 'no' => $no, 'image_url' => $image_url, 'share_url' => $share_url));
}
else
{
    $data = array('result' => false, 'err_code' => $err_code, 'message' => $err);
}

$response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
$response->setData($data);
//$response->setCallback('handleResponse');
$response->send();