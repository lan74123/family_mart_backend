<?php
require 'vendor/autoload.php';

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Medoo\Medoo;

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();
$response = new JsonResponse();
$db = new Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
	'password' => getenv('DB_PASSWORD'),
]);

$result = true;
$err = '';

$time_add_people = 0;

$time_point = new DateTime('2018-02-27 00:00:00');
$time_now = new DateTime('2018-03-01 01:40:00');
if ($time_now > $time_point)
{
	$diff = $time_point->diff($time_now);

	$minutes = $diff->days * 24 * 60;
	$minutes += $diff->h * 60;
	$minutes += $diff->i;

	$time_add_people = floor($minutes / 5) * 5;
}

$count = $db->count('user_logs') + 100 + $time_add_people;
$top20 = $db->select('user_logs', ["image" => Medoo::raw("CONCAT('".getenv('APP_URL')."user/', <fbid>, '/', <result_image>)")], ['ORDER' => Medoo::raw('RAND()'), 'LIMIT' => 20]);

$data = array('succ' => $result, 'message' => $err, 'result' => array('count' => $count, 'top20' => $top20));

$response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
$response->setData($data);
//$response->setCallback('handleResponse');
$response->send();