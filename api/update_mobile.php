<?php
require '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

$dotenv = new Dotenv\Dotenv(__DIR__. '/..');
$dotenv->load();
$response = new JsonResponse();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
]);

$server = getenv('APP_URL');
$error_message = '';
$result = false;
$ts = time();
$playerID = 0;

$user_path = '';

session_start();

$checkCode = $_POST['checkCode'];
if(!isset($_SESSION))$SEC = "";
else $SEC = $_SESSION['checkNum'];  

//如果驗證碼為空
if($checkCode == "")
{
    $data = array('result' => "N", 'ErrorMessage' => "請輸入驗證碼");
    $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
    $response->setData($data);
    //$response->setCallback('handleResponse');
    $response->send();

}
//如果驗證碼不是空白但輸入錯誤
else if($checkCode != $SEC && $checkCode !="")
{
    $data = array('result' => "N", 'ErrorMessage' => "驗證碼錯誤，請重新輸入或點擊驗證碼圖片以更新驗證碼");
    $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
    $response->setData($data);
    //$response->setCallback('handleResponse');
    $response->send();
}
else{
    //驗證碼輸入正確
    $client_referer = $_SERVER['HTTP_REFERER'];
    $offical_web = 'https://www.familymart30.com.tw/recodePage.html';
    $demo_web = 'http://www.our-work.com.tw/code/2018FamilyMart/recodePage.html';
    if($client_referer != $demo_web && $client_referer != $offical_web){
        $_SESSION['checkNum'] = '';
        $data = array('result' => "N", 'ErrorMessage' => '驗證失敗');
        $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
        $response->setData($data);
        //$response->setCallback('handleResponse');
        $response->send();
        exit();
    }    
    if ($request->getMethod() == 'POST')
    {
        $playerID = $request->request->get('playerID');
        $mobile = $request->request->get('mobile');
        
        if (!$request->request->has('mobile'))
        {
            $error_message .= '請輸入電話\\n';
        }
        else if (!preg_match('/^(09)\d{8}$/',$mobile)){
            $error_message .= '電話格式不符';
        }
        if (!$request->request->has('playerID'))
        {
            $error_message .= '參數錯誤\\n';
        }

        if (empty($error_message))
        {
            $db->update('player', [
                'mobile' => $mobile
            ],
            [
                'id' => $playerID
            ]);
            
            $result = "Y";
        }
    }
    else
    {
        $error_message = 'Method not allow';
    }

    if (empty($error_message))
    {
        $data = array('Result' => $result, 
        'playerID' => $playerID, 
        'ErrorMessage' => $error_message);
    }
    else
    {
        $data = array('Result' => "N", 
        'ErrorMessage' => $error_message);
    }
    $_SESSION['checkNum'] = '';

    $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
    $response->setData($data);
    //$response->setCallback('handleResponse');
    $response->send();
}