<?php
require '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

$dotenv = new Dotenv\Dotenv(__DIR__. '/..');
$dotenv->load();
$response = new JsonResponse();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
]);

$server = getenv('APP_URL');
$error_message = '';
$result = false;
$playerID = 0;

$user_path = '';
    if ($request->getMethod() == 'POST')
    {
        $client_referer = $_SERVER['HTTP_REFERER'];
        $offical_web = 'https://www.familymart30.com.tw/recodePage.html';
        $demo_web = 'http://www.our-work.com.tw/code/2018FamilyMart/recodePage.html';
        if($client_referer != $demo_web && $client_referer != $offical_web){
            $data = array('result' => "N", 'ErrorMessage' => '驗證失敗');
            $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
            $response->setData($data);
            //$response->setCallback('handleResponse');
            $response->send();
            exit();
        }    
        if (!$request->request->has('playerID'))
        {
            $error_message .= '參數錯誤\\n';
        }

        if (!$request->request->has('FBName'))
        {
            $error_message .= '參數錯誤\\n';
        }
        
        if (!$request->request->has('FBPic'))
        {
            $error_message .= '抓取臉書大頭貼失敗\\n';
        }
        
        if (!$request->request->has('FBID'))
        {
            $error_message .= '參數錯誤\\n';
        }

        //STEP1 Save Image
        if (empty($error_message))
        {
            $playerID = $request->request->get('playerID');
            $FBName = $request->request->get('FBName');
            $FBPic = $request->request->get('FBPic');
            $FBID = $request->request->get('FBID');
            $rawImage = file_get_contents($FBPic);
            //$data = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $FBPic));
            $pic_path = '';
            if (!empty($rawImage)) {
                //file_put_contents(__DIR__. '/../storage/FBpic/'.$playerID.'.jpg', $data);
                file_put_contents(__DIR__. '/../storage/FBpic/'.$playerID.'.jpg', $rawImage);
                $pic_path = getenv('APP_URL').'/storage/FBpic/'.$playerID.'.jpg';
            }

            $db->update('player', [
                'fbname' => $FBName,
                'fbpic' => $pic_path,
                'fbshare' => '1',
                'fbid' => $FBID,
                'fbsharedate' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => $playerID
            ]);
            
            $result = "Y";

        }
    }
    else
    {
        $error_message = 'Method not allow';
    }

    if (empty($error_message))
    {
        $data = array('Result' => $result, 
        'pic_path' => $pic_path,
        'ErrorMessage' => $error_message);
    }
    else
    {
        $data = array('Result' => "N", 
        'ErrorMessage' => $error_message);
    }

    $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
    $response->setData($data);
    //$response->setCallback('handleResponse');
    $response->send();
