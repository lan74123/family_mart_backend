<?php
namespace Azure\Face;

class Face {

    private $apikey = array('aa78f5f985504ae3aefa1adcbc4c5ec3', '941b25911d104db78c0a11032b707b59');

    public $result;
    public $message;
    public $image_url;
    public $age;
    public $top;
    public $left;
    public $gender;
    public $width;
    public $height;

    function __construct($image_url)
    {
        $this->image_url = $image_url;
    }

    function detect()
    {
        $client = new \GuzzleHttp\Client();

        $headers = array(
            'Content-Type' => 'application/json',
            'Ocp-Apim-Subscription-Key' => $this->apikey[rand(0, 1)],
        );

        $parameters = array(
            'returnFaceId' => 'true',
            'returnFaceLandmarks' => 'false',
            'returnFaceAttributes' => 'age,gender',
        );

        $body = '{"url": "'.$this->image_url.'"}';

        try
        {
            $response = $client->request('POST', 'https://westus.api.cognitive.microsoft.com/face/v1.0/detect', ['body' => $body, 'headers' => $headers, 'query' => $parameters]);

            if ($response->getStatusCode() == 200 && $response->getReasonPhrase() == 'OK')
            {
                $result = json_decode($response->getBody());

                if (count($result) > 1)
                {
                    $this->result = false;
                    $this->message = 'E01'; //偵測到多張臉孔
                }
                else if (count($result) == 0)
                {
                    $this->result = false;
                    $this->message = 'E00'; //未偵測到臉孔
                }
                else
                {
                    $this->result = true;
                    $this->age = floor($result[0]->faceAttributes->age);
                    $this->gender = $result[0]->faceAttributes->gender;
                    $this->top = $result[0]->faceRectangle->top;
                    $this->left = $result[0]->faceRectangle->left;
                    $this->width = $result[0]->faceRectangle->width;
                    $this->height = $result[0]->faceRectangle->height;
                }
            }
            else
            {
                $this->result = false;
                $this->message = 'E00';
            }
        }
        catch (\Exception $e)
        {
            $this->result = false;
            $this->message = 'E00'; //未偵測到臉孔
        }
        
    }
}