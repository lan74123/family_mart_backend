<?php
require '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

$dotenv = new Dotenv\Dotenv(__DIR__. '/..');
$dotenv->load();
$response = new JsonResponse();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
]);

if (!empty($_SERVER["HTTP_CLIENT_IP"])){
    $ip = $_SERVER["HTTP_CLIENT_IP"];
}elseif(!empty($_SERVER["HTTP_X_FORWARDED_FOR"])){
    $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];
}else{
    $ip = $_SERVER["REMOTE_ADDR"];
}

$server = getenv('APP_URL');
$error_message = '';
$result = false;
$ts = time();
$playerID = 0;

$user_path = '';

if ($request->getMethod() == 'POST')
{
    $client_referer = $_SERVER['HTTP_REFERER'];
    $offical_web = 'https://www.familymart30.com.tw/recodePage.html';
    $demo_web = 'http://www.our-work.com.tw/code/2018FamilyMart/recodePage.html';
    if($client_referer != $demo_web && $client_referer != $offical_web){
        $data = array('result' => "N", 'ErrorMessage' => '驗證失敗');
        $response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
        $response->setData($data);
        //$response->setCallback('handleResponse');
        $response->send();
        exit();
    }
    if (!$request->request->has('name'))
    {
        $error_message .= '請輸入暱稱\\n';
    }
    if (!$request->request->has('content'))
    {
        $error_message .= '參數錯誤\\n';
    }

    //STEP1 Save Image
    if (empty($error_message))
    {
        $name = $request->request->get('name');
        $content = $request->request->get('content');
        $musictype = $request->request->has('musictype') ? $request->request->get('musictype') : '';
        $animationtype = $request->request->has('animationtype') ? $request->request->get('animationtype') : '';
        $utm_source = $request->request->has('utm_source') ? $request->request->get('utm_source') : '';
        $utm_medium = $request->request->has('utm_medium') ? $request->request->get('utm_medium') : '';
        $utm_campaign = $request->request->has('utm_campaign') ? $request->request->get('utm_campaign') : '';

    }

    if (empty($error_message))
    {

            $db->insert('player', [
                'name' => $name,
                'content' => $content,
                'musictype' => $musictype,
                'animationtype' => $animationtype,
                'utm_source' => $utm_source,
                'utm_medium' => $utm_medium,
                'utm_campaign' => $utm_campaign,
                'created_at' => date('Y-m-d H:i:s'),
                // 'ip' => $request->getClientIp()
                'ip' => $ip
            ]);
            $result = "Y";
            $playerID = $db->id();

    }
}
else
{
    $error_message = 'Method not allow';
}

if (empty($error_message))
{
    $data = array('Result' => $result, 'playerID' => $playerID, 'ErrorMessage' => $error_message);
}
else
{
    $data = array('Result' => "N", 'ErrorMessage' => $error_message);
}

$response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
$response->setData($data);
//$response->setCallback('handleResponse');
$response->send();