<?php
require '../vendor/autoload.php';

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

$dotenv = new Dotenv\Dotenv(__DIR__. '/..');
$dotenv->load();
$response = new JsonResponse();
$request = Request::createFromGlobals();
$db = new Medoo\Medoo([
    'database_type' => 'mysql',
	'database_name' => getenv('DB_NAME'),
	'server' => getenv('DB_IP'),
	'username' => getenv('DB_USER'),
    'password' => getenv('DB_PASSWORD'),
    'charset' => 'utf8',
]);
// $fb = new \Facebook\Facebook([
//     'app_id' => getenv('APP_ID'),
//     'app_secret' => getenv('APP_SECRET'),
//     'default_graph_version' => 'v2.11',
//     'default_access_token' => getenv('APP_ID') . '|' . getenv('APP_SECRET'),
// ]);

$server = getenv('APP_URL');
$error_message = '';
$result = false;
$ts = time();
$playerID = 0;

$user_path = '';
if ($request->getMethod() == 'POST')
{
    if (empty($error_message))
    {
        $playerID = $request->request->get('playerID');
    }

    if (empty($error_message))
    {

            $datas = $db->select('player', [
                'content',
                'valid'
            ],
            [
                'id' => $playerID
            ]);
            
            $result = "Y";
            $content = $datas[0]["content"];
            if(empty($content)){
                $error_message = "參數錯誤";
            }
            if($datas[0]["valid"] == 0){
                $error_message = "作品已下架";
            }
    }
}
else
{
    $error_message = 'Method not allow';
}

if (empty($error_message))
{
    $data = array('Result' => $result, 
    'playerID' => $playerID,
    'content' => $content,
    'ErrorMessage' => $error_message);
}
else
{
    $data = array('Result' => "N", 
    'ErrorMessage' => $error_message);
}

$response->headers->set('Access-Control-Allow-Origin', getenv('APP_HOST'));
$response->setData($data);
//$response->setCallback('handleResponse');
$response->send();
